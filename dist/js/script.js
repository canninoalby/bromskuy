// Navbar Fixed
window.onscroll = function () {
  const header = document.querySelector("header");
  const bottomnav = document.querySelector("#bottom-navigation");
  const fixednav = header.offsetTop;
  const toTop = document.querySelector("#to-top");

  if (window.pageYOffset > fixednav) {
    header.classList.add("navbar-fixed");
    bottomnav.classList.add("navbar-bottom");
    toTop.classList.remove("hidden");
    toTop.classList.add("flex");
  } else {
    header.classList.remove("navbar-fixed");
    bottomnav.classList.remove("navbar-bottom");
    toTop.classList.remove("flex");
    toTop.classList.add("hidden");
  }
};
