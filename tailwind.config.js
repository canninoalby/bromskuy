/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin')
module.exports = {
  content: ["./about.html", "./index.html", "./dos.html", "./project.html", "./*.html"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {
        primary: "#ffefe0",
        secondary: "#b5865",
        neutral: "#f3f4f6",
        vercel: "#0284c7",
        netlify: "#2563eb",
        github: "#57534e",
        coffee: "#d97706",
        kofi: "#db2777",
        dark: "#262626",
        backgroundImage: {
          'bg1': "url('dist/img/backgrounds/img1.png')",
          'bg2': "url('dist/img/backgrounds/img2.png')",
          'bg3': "url('dist/img/backgrounds/img3.png')",
          'bg4': "url('dist/img/backgrounds/img4.png')",
          'bg5': "url('dist/img/backgrounds/img5.png')",
          'bg6': "url('dist/img/backgrounds/img6.png')",
          'bg7': "url('dist/img/backgrounds/img7.png')",
        }
      },
      textShadow: {
        sm: '0 1px 2px var(--tw-shadow-color)',
        DEFAULT: '0 2px 4px var(--tw-shadow-color)',
        lg: '0 8px 16px var(--tw-shadow-color)',
      },
      screens: {
        xl: "1320px",
      },
      fontFamily: {
        poppins: ["Cousine", "sans-serif"],
      },
    },
  },
  plugins: [
    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          'text-shadow': (value) => ({
            textShadow: value,
          }),
        },
        { values: theme('textShadow') }
      )
    }),
  ],
};
